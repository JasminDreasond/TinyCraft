# TinyCraft

A tiny puddy craft made by Tiny Jasmini. :3

## Installation

- Unzip the archive, rename the folder to `tinycraft` and
place it in .. `minetest/games/`

- GNU/Linux: If you use a system-wide installation place it in `~/.minetest/games/`.

- Install modules:
```sh
yarn
```

## Licensing

See `LICENSE.txt`

### Credits

Water and Fire texture - Vilja Pix 2.0 

Lava texture - PixelBOX