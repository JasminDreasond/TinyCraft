import path from 'path';
import fs from 'fs-extra';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

console.log(`[tinycraft] Starting install...`);

const folders = [
    'modules',
];

const tinyModules = [

    // Blocks
    { path: 'invisiblocks', module: 'invisiblocks' },
    { path: 'moreblocks', module: 'moreblocks' },
    { path: 'moreores', module: 'moreores' },
    { path: 'nextgen_bedrock', module: 'nextgen_bedrock' },
    { path: 'placeable_buckets', module: 'placeable_buckets' },
    { path: 'spawners', module: 'spawners' },
    { path: 'xdecor', module: 'xdecor-libre' },
    { path: 'gates_long', module: 'gates_long' },
    { path: 'x_clay', module: 'x_clay' },
    { path: 'x_enchanting', module: 'x_enchanting' },
    { path: 'craft_table', module: 'craft_table' },
    { path: 'mesecons', module: 'mesecons' },

    // Essentials
    { path: 'awards', module: 'awards' },
    { path: 'people', module: 'people' },
    { path: 'skinsdb', module: 'skinsdb' },

    // Furniture
    { path: 'morelights', module: 'morelights' },
    { path: 'smoke_signals', module: 'smoke_signals' },

    // Items
    { path: 'pep', module: 'minetest_pep' },
    { path: 'sailing_kit', module: 'sailing_kit' },
    { path: 'minecart', module: 'minecart' },
    { path: 'bonemeal', module: 'bonemeal' },
    { path: 'enderpearl', module: 'enderpearl' },
    { path: 'signs_lib', module: 'signs_lib' },
    { path: '3d_armor', module: '3d_armor' },

    // Map Gen
    { path: 'dungeonflora', module: 'mintest-dungeon-flora' },
    { path: 'treeparticles', module: 'mod_treeparticles' },
    { path: 'nether', module: 'nether' },
    { path: 'nssb', module: 'nssb' },
    { path: 'marinara', module: 'marinara' },

    // Sky
    { path: 'lightning', module: 'lightning' },

    // Mobs
    { path: 'goblins', module: 'goblins' },
    { path: 'creeper', module: 'creeper' },
    { path: 'wisp', module: 'wisp' },
    { path: 'nether_mobs', module: 'minetest-nether-monsters' },
    { path: 'animalworld', module: 'animalworld' },
    { path: 'banshee', module: 'banshee' },
    { path: 'forgotten_monsters', module: 'forgotten_monsters' },
    { path: 'livingnether', module: 'livingnether' },
    { path: 'mobs_animal', module: 'mobs_animal' },
    { path: 'mobs_monster', module: 'mobs_monster' },
    { path: 'mobs_npc', module: 'mobs_npc' },
    { path: 'mobs_skeletons', module: 'mobs_skeletons' },
    { path: 'nativevillages', module: 'nativevillages' },
    { path: 'mobs_water', module: 'mobs_water' },
    { path: 'mobs_ghost_redo', module: 'mobs_ghost_redo' },
    { path: 'nssm', module: 'nssm' },
    { path: 'dmobs', module: 'dmobs' },

    // Weapons
    { path: 'x_bows', module: 'x_bows' },
    { path: 'x_bows_extras', module: 'x_bows_extras' },
    { path: 'x_obsidianmese', module: 'x_obsidianmese' },

    // Libs
    { path: 'playereffects', module: 'minetest_playereffects' },
    { path: 'player_settings', module: 'player_settings' },
    { path: 'flow', module: 'minetest-flow' },
    { path: 'formspec_ast', module: 'minetest-formspec_ast' },
    { path: 'controls', module: 'controls' },
    { path: 'mob_core', module: 'mob_core' },
    { path: 'mobkit', module: 'mobkit' },
    { path: 'mobs_redo', module: 'mobs_redo' },
    { path: 'xconnected', module: 'xconnected' },
    { path: 'modlib', module: 'modlib' },
    { path: 'playerpshysics_monoid', module: 'minetest_playerphysics' },
    { path: 'player_monoids', module: 'player_monoids' },

    // Cave
    { path: 'livingcaves', module: 'livingcaves' },
    { path: 'livingcavesmobs', module: 'livingcavesmobs' },
    { path: 'karst_caverns', module: 'karst_caverns' },
    { path: 'stalker', module: 'Stalker' },

];

const modulesFolder = path.join(__dirname, '../mods/modules');
if (fs.existsSync(modulesFolder)) {
    console.log(`[tinycraft] Removing cache...`);
    fs.rmSync(modulesFolder, { recursive: true, force: true });
}

console.log(`[tinycraft] Cache removed!`);

for (const item in folders) {

    const folder = path.join(__dirname, '../mods/' + folders[item]);
    console.log(`[tinycraft] [checking] ${folder}`);

    if (!fs.existsSync(folder)) {
        fs.mkdirSync(folder);
    }

}

console.log(`[tinycraft] Check complete!`);

console.log(`[tinycraft] Creating external modules file...`);
fs.writeFileSync(path.join(modulesFolder, './modpack.conf'), 'name = EXTERNALMODULES\ndescription = TinyCraft Stuff', 'utf-8');

console.log(`[tinycraft] Preparing module files...`);
for (const item in tinyModules) {

    const tinyModule = path.join(__dirname, '../node_modules/' + tinyModules[item].module);
    const folder = path.join(modulesFolder, './' + tinyModules[item].path);

    console.log(`[tinycraft] [installing] ${folder}`);
    fs.copySync(tinyModule, folder, { overwrite: true });

}

console.log(`[tinycraft] Install progress complete!`);
console.log(`[tinycraft] Tiny done! :3`);