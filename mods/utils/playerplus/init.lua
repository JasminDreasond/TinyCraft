--[[
	walking on ice makes player walk faster,
	stepping through snow slows player down,
	touching a cactus hurts player,
	suffocation when head is inside solid node,
	player knock-back effects when punched.

	PlayerPlus by TenPlus1
]]

playerplus = {}


-- cache if player_monoids mod active?
local monoids = minetest.get_modpath("player_monoids")
local pova_mod = minetest.get_modpath("pova")


-- get node but use fallback for nil or unknown
local node_ok = function(pos, fallback)

	local node = minetest.get_node_or_nil(pos)

	if node and minetest.registered_nodes[node.name] then
		return node.name
	end

	return fallback or "air"
end


local armor_mod = minetest.get_modpath("3d_armor")
local time = 0


minetest.register_globalstep(function(dtime)

	time = time + dtime

	-- every 1 second
	if time < 1 then
		return
	end

	-- reset time for next check
	time = 0

	-- define locals outside loop
	local name, pos, ndef, def, nslow, nfast, prop

	-- get list of players
	local players = minetest.get_connected_players()

	-- loop through players
	for _,player in pairs(players) do

		-- who am I?
		name = player:get_player_name()

if name and playerplus[name] then

		-- where am I?
		pos = player:get_pos()

		-- what is around me?
		playerplus[name].nod_stand = node_ok({
			x = pos.x, y = pos.y - 0.1, z = pos.z})

		-- Does the node below me have an on_walk_over function set?
		ndef = minetest.registered_nodes[playerplus[name].nod_stand]
		if ndef and ndef.on_walk_over then
			ndef.on_walk_over(pos, ndef, player)
		end

		prop = player:get_properties()

		-- node at eye level
		playerplus[name].nod_head = node_ok({
			x = pos.x, y = pos.y + prop.eye_height, z = pos.z})

		-- node at foot level
		playerplus[name].nod_feet = node_ok({
			x = pos.x, y = pos.y + 0.2, z = pos.z})

		-- Is player suffocating inside a normal node without no_clip privs?
		local ndef = minetest.registered_nodes[playerplus[name].nod_head]

		if ndef.walkable == true
		and ndef.drowning == 0
		and ndef.damage_per_second <= 0
		and ndef.groups.disable_suffocation ~= 1
		and ndef.drawtype == "normal"
		and not minetest.check_player_privs(name, {noclip = true}) then

			if player:get_hp() > 0 then
				player:set_hp(player:get_hp() - 2, {suffocation = true})
			end
		end

		-- am I near a cactus?
		local near = minetest.find_node_near(pos, 1, "default:cactus")

		if near then

			-- am I touching the cactus? if so it hurts
			for _,object in pairs(minetest.get_objects_inside_radius(near, 1.1)) do

				object:punch(object, 0.1, {
					full_punch_interval = 0.1,
					damage_groups = {fleshy = 2}
				}, near)
			end

		end

end -- END if name

	end
end)


-- check for old sneak_glitch setting
local old_sneak = minetest.settings:get_bool("old_sneak")

-- set to blank on join (for 3rd party mods)
minetest.register_on_joinplayer(function(player)

	local name = player:get_player_name()

	playerplus[name] = {nod_head = "", nod_feet = "", nod_stand = ""}

	-- apply old sneak glitch if enabled
	if old_sneak then
		player:set_physics_override({new_move = false, sneak_glitch = true})
	end
end)


-- clear when player leaves
minetest.register_on_leaveplayer(function(player)

	playerplus[ player:get_player_name() ] = nil
end)


-- add privelage to disable knock-back
minetest.register_privilege("no_knockback", {
	description = "Disables player knock-back effect",
	give_to_singleplayer = false
})

-- is player knock-back effect enabled?
if minetest.settings:get_bool("player_knockback") == true then

-- player knock-back function
local punchy = function(
		player, hitter, time_from_last_punch, tool_capabilities, dir, damage)

	local name = player:get_player_name()

	-- is player attached to anything (mob, bed, boat etc.)
	if player_api.player_attached[name] then
		return
	end

	if not dir then return end

	-- check if player has 'no_knockback' privelage
	local privs = minetest.get_player_privs(name)

	if privs["no_knockback"] then
		return
	end

	local damage = 0

	-- get tool damage
	if tool_capabilities then

		local armor = player:get_armor_groups() or {}
		local tmp

		for group,_ in pairs( (tool_capabilities.damage_groups or {}) ) do

			tmp = time_from_last_punch / (tool_capabilities.full_punch_interval or 1.4)

			if tmp < 0 then
				tmp = 0.0
			elseif tmp > 1 then
				tmp = 1.0
			end

			damage = damage + (tool_capabilities.damage_groups[group] or 0) * tmp
		end

		-- check for knockback value
		if tool_capabilities.damage_groups["knockback"] then
			damage = tool_capabilities.damage_groups["knockback"]
		end

	end
	-- END tool damage

	local kb = math.min(32, damage * 2)

--	print ("---", player:get_player_name(), damage, kb)

	-- knock back player
	player:add_velocity({
		x = dir.x * kb,
		y = -1,
		z = dir.z * kb
	})
end

minetest.register_on_punchplayer(punchy)

end -- END if

minetest.register_tool("playerplus:stick", {
	description = "Sword of Boing",
	inventory_image = "playerplus_sword_boing.png",
	wield_image = "playerplus_sword_boing.png",
	tool_capabilities = {
		full_punch_interval = 1.4,
		damage_groups = {fleshy = 0, knockback = 11, not_in_creative_inventory = 1}
	}
})

--[[
minetest.override_item("default:mese", {
	on_walk_over = function(pos, node, player)
		print ("---", node.name, player:get_player_name() )
	end
})
]]


-- add lucky blocks (if damage and stamina active)
if minetest.get_modpath("lucky_block") then

	local MP = minetest.get_modpath(minetest.get_current_modname())

	dofile(MP .. "/lucky_block.lua")
end
